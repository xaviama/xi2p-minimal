#!/bin/sh

termination() {
    printf "exiting..."
    if ! [ -z $I2P_PID ]; then
	    kill -9 $I2P_PID
    fi
}

trap "termination" TERM INT

SCRIPT_PATH="$(dirname "$(realpath "$0")")"

cd "$SCRIPT_PATH"

if ! [ -d "data/family" ]; then
	mkdir -p data/family
fi

if ! [ -f "data/family/XaviaMa.key" ]; then
	cp keys/XaviaMa.key data/family/
fi

if ! [ -d "data/certificates/family" ]; then
	mkdir -p data/certificates/family
fi

if ! [ -f "data/certificates/family/XaviaMa.crt" ]; then
	cp keys/XaviaMa.crt data/certificates/family/
fi

if ! [ -d "data/certificates/reseed" ]; then
	mkdir -p "data/certificates/reseed"
fi

if ! [ -f "data/certificates/reseed/XaviaMa.crt" ]; then
	cp keys/XaviaMa.crt data/certificates/reseed/
fi

i2pd \
	--conf="$SCRIPT_PATH/i2pd.conf" \
	--datadir="$SCRIPT_PATH/data"
I2P_PID=$!
